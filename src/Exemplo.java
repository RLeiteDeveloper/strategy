package src;

import java.util.Scanner;

import src.service.Frete;
import src.service.TipoFrete;

public class Exemplo {

	public static void main(String[] args) {
		try (Scanner entrada = new Scanner(System.in)) {
			System.out.print("Informe a distância: ");
			int distancia = entrada.nextInt();
			System.out.print("Qual o tipo de frete (1) Normal, (2) Sedex, (3) Programada: ");
			int opcaoFrete = entrada.nextInt();
			TipoFrete tipoFrete = TipoFrete.values()[opcaoFrete - 1];
			
			Frete frete = tipoFrete.builderObterFrete();
			double preco = frete.calcularFrete(distancia);
			System.out.printf("O valor total é de R$%.2f", preco);
		}
	}
	
}
