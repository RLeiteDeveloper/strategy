package src.service;

import src.service.frete.Normal;
import src.service.frete.Programada;
import src.service.frete.Sedex;

public enum TipoFrete {
    
   NORMAL {
    @Override
    public Frete builderObterFrete() {
            return new Normal();
        }
    },
    
   SEDEX {
    @Override
    public Frete builderObterFrete() {
        return new Sedex();
        }
    },

    PROGRAMADA {
        @Override
        public Frete builderObterFrete() {
            return new Programada();
        }
    };

    public abstract Frete builderObterFrete(); 
    
}
