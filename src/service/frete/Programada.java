package src.service.frete;

import src.service.Frete;

public class Programada implements Frete {

    @Override
    public double calcularFrete(int distancia) {
        return distancia * 1.65 + 13;
    }
    
}
