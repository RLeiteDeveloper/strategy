package src.service.frete;

import src.service.Frete;

public class Normal implements Frete {

    @Override
    public double calcularFrete(int distancia) {
        return distancia * 1.25 + 10;
    }


    
}
