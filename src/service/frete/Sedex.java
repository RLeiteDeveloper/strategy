package src.service.frete;

import src.service.Frete;

public class Sedex implements Frete {

    @Override
    public double calcularFrete(int distancia) {
        return distancia * 1.45 + 12;
    }
    
}
